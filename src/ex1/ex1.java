package ex1;

public class ex1 {

    class A extends B {

    }

    class B {
        private long t;

        public int b() {
            return 0;
        }
    }

    class C {
        B b;

        public void doSomething() {
            b = new B();
        }
    }

    class D {
        private F f;
        private final E e;

        public D(E e) {
            this.e = e;
        }

        public void met1(int i) {

        }
    }

    class E {
        public int met2() {
        }
    }

    class F {
        public void n(String s) {

        }
    }
}
