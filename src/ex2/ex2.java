package ex2;

import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class ex2 {
    private JTextField textField1;
    private JButton button1;
    private JTextArea textArea1;
    private JPanel panel;

    public ex2() {
        textField1.setText("file");
        button1.addActionListener(e -> {
            try {
                PrintWriter out = new PrintWriter(new FileWriter("file.txt"));
                textArea1.write(out);
                out.close();
            } catch (IOException ioException) {
                System.out.printf("Error occurred");
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ex2");
        frame.setContentPane(new ex2().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
